# autocell
`autocell` is a terminal-based cellular automata simulator. It uses ncurses for its "graphics" and the simulation space is bounded by the terminal it's running in.

## Input Format
By convention, `autocell` input files end in the `.ac` extension. This is not required.

Input files begin with a single integer, corresponding to the ruleset to be used. Following that integer, input files contain a set of whitespace-delimited signed-integer pairs that define coordinates relative to the center of the simulation space. The coordinate axes are aligned such that **-x points left** and **+y points up**.

Examples may be found in the [examples](/examples) directory. The source ruleset enumeration may be found in [src/ruleset.h](/src/ruleset.h).

| Ruleset Enumeration | Meaning                    |
|---------------------|----------------------------|
| `0`                 | [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) |
| `1`                 | Falling Blocks |
| `2`                 | [Brian's Brain](https://en.wikipedia.org/wiki/Brian's_Brain) |
| `3`                 | [WireWorld](https://en.wikipedia.org/wiki/Wireworld) |

The examples use line-separated tuples for better readability, however this isn't required. Any amount of whitespace between tuples is valid.

The first two numbers of an input tuple are (x,y) coordinates, relative to the center of the terminal. The third number is the state to initialize the cell to (should be a valid state in the ruleset specified at beginning of the file).

| Input Tuple     | Meaning                        |
|-----------------|--------------------------------|
| `0 0  1`        | Initialize center cell of the terminal to state **1** |
| `0 -1 2`        | Initialize one cell below center to state **2** |
| `-1 0 3`        | Initialize cell to the left of center to state **3** |
| ...             | ...                            |

## Usage
To compile `autocell`, simply run `make` in this repo's base directory. The executable will be stored in the `build` directory.

`autocell` has one positional argument: the input file.

Pressing any key will cause the simulation to step forward one generation. Holding down a key will result in continuous simulation.

Pressing `[Ctrl-D]` will terminate the program.

### Example Usage
`build/autocell examples/conway/glider.ac`
