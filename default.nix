with import <nixpkgs> {};

stdenv.mkDerivation {
	pname = "autocell";
	version = "0.1.0";
	buildInputs = [
          gcc
          ncurses
	];
	src = ./.;
}
