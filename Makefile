CC		= gcc
CFLAGS		= -Werror -Wall -Wstrict-prototypes -Wundef -Wmissing-declarations -Wmissing-prototypes -std=gnu17 -fsanitize=address
LDFLAGS		= -lncurses
SRCDIR		= src
BUILDDIR	= build
PROGNAME	= autocell

$(BUILDDIR)/$(PROGNAME): $(BUILDDIR) $(SRCDIR)/*
	$(CC) $(CFLAGS) $(LDFLAGS) $(SRCDIR)/*.c -o $@

$(BUILDDIR):
	mkdir -p $(BUILDDIR)

.PHONY: clean
clean:
	rm -r $(BUILDDIR)
