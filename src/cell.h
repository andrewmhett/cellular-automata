#ifndef CELL_H
#define CELL_H

#include "cell.h"
#include "states.h"
#include "ruleset.h"

typedef struct Cell {
    unsigned state;
    struct Cell* neighbors[3][3];
} Cell;

Cell step_cell(Cell, CARuleset);

#endif