#include <stddef.h>

#include "cell.h"
#include "states.h"
#include "ruleset.h"

Cell step_cell(Cell current_cell, CARuleset rules) {
    unsigned orig_state = current_cell.state;
    Cell new_cell = current_cell;

    switch (rules) {
        case CONWAY:
            unsigned num_living_neighbors = 0;
            for (int row = 0; row < 3; row++) {
                for (int col = 0; col < 3; col++) {
                    if (current_cell.neighbors[row][col] == NULL) continue;
                    if (current_cell.neighbors[row][col]->state == CGL_ALIVE) num_living_neighbors++;
                }
            }

            switch (orig_state) {
                case CGL_DEAD:
                    if (num_living_neighbors == 3) {
                        new_cell.state = CGL_ALIVE;
                    }
                    break;
                case CGL_ALIVE:
                    if (num_living_neighbors < 2 || num_living_neighbors > 3) {
                        new_cell.state = CGL_DEAD;
                    }
                    break;
            }
            break;

        case FALLING_BLOCKS:
            switch (orig_state) {
                case FB_EMPTY:
                    if (current_cell.neighbors[0][1] == NULL) break;
                    if (current_cell.neighbors[0][1]->state == FB_BLOCK) new_cell.state = FB_BLOCK;
                    break;
                case FB_BLOCK:
                    if (current_cell.neighbors[2][1] == NULL) break;
                    if (current_cell.neighbors[2][1]->state == FB_EMPTY) new_cell.state = FB_EMPTY;
                    break;
            }
            break;

        case BRIANS_BRAIN:
            unsigned num_alive_neighbors = 0;
            for (int row = 0; row < 3; row++) {
                for (int col = 0; col < 3; col++) {
                    if (current_cell.neighbors[row][col] == NULL) continue;
                    if (current_cell.neighbors[row][col]->state == BB_ALIVE) num_alive_neighbors++;
                }
            }

            switch (orig_state) {
                case BB_DEAD:
                    if (num_alive_neighbors == 2) new_cell.state = BB_ALIVE;
                    break;
                case BB_ALIVE:
                    new_cell.state = BB_DYING;
                    break;
                case BB_DYING:
                    new_cell.state = BB_DEAD;
                    break;
            }
            break;

        case WIRE_WORLD:
            switch (orig_state) {
                case WW_EMPTY:
                    break;
                case WW_ELECTRON_HEAD:
                    new_cell.state = WW_ELECTRON_TAIL;
                    break;
                case WW_ELECTRON_TAIL:
                    new_cell.state = WW_CONDUCTOR;
                    break;
                case WW_CONDUCTOR:
                    unsigned num_head_neighbors = 0;
                    for (int row = 0; row < 3; row++) {
                        for (int col = 0; col < 3; col++) {
                            if (current_cell.neighbors[row][col] == NULL) continue;
                            if (current_cell.neighbors[row][col]->state == WW_ELECTRON_HEAD) num_head_neighbors++;
                        }
                    }

                    if (num_head_neighbors > 0 && num_head_neighbors <= 2) new_cell.state = WW_ELECTRON_HEAD;
                    break;
            }
    }

    return new_cell;
};
