#ifndef STATES_H
#define STATES_H

typedef enum {
    CGL_DEAD,
    CGL_ALIVE
} ConwayState;

typedef enum {
    FB_EMPTY,
    FB_BLOCK
} FallingBlockState;

typedef enum {
    BB_DEAD,
    BB_ALIVE,
    BB_DYING
} BriansBrainState;

typedef enum {
    WW_EMPTY,
    WW_ELECTRON_HEAD,
    WW_ELECTRON_TAIL,
    WW_CONDUCTOR
} WireWorldState;

#endif