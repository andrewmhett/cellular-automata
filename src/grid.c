#include <string.h>
#include <regex.h>
#include <assert.h>
#include <stdlib.h>

#include "grid.h"
#include "cell.h"

unsigned grid_index(unsigned row, unsigned col, unsigned width) {
    return row * width + col;
}

Cell* init_grid(char* grid_input, long in_length, unsigned rows, unsigned cols) {
    regex_t input_tuple_regp;
    assert(regcomp(&input_tuple_regp, INPUT_TUPLE_REGEX, REG_EXTENDED) == 0);

    Cell* grid = malloc(rows * cols * sizeof(Cell));

    for (unsigned row = 0; row < rows; row++) {
        for (unsigned col = 0; col < cols; col++) {
            Cell new_cell;
            new_cell.state = 0;
            new_cell.neighbors[0][0] = grid + grid_index(row - 1, col - 1, cols);
            new_cell.neighbors[0][1] = grid + grid_index(row - 1, col, cols);
            new_cell.neighbors[0][2] = grid + grid_index(row - 1, col + 1, cols);
            new_cell.neighbors[1][0] = grid + grid_index(row, col - 1, cols);
            new_cell.neighbors[1][1] = NULL;
            new_cell.neighbors[1][2] = grid + grid_index(row, col + 1, cols);
            new_cell.neighbors[2][0] = grid + grid_index(row + 1, col - 1, cols);
            new_cell.neighbors[2][1] = grid + grid_index(row + 1, col, cols);
            new_cell.neighbors[2][2] = grid + grid_index(row + 1, col + 1, cols);
            if (row == 0) {
                new_cell.neighbors[0][0] = NULL;
                new_cell.neighbors[0][1] = NULL;
                new_cell.neighbors[0][2] = NULL;
            }
            if (row == rows - 1) {
                new_cell.neighbors[2][0] = NULL;
                new_cell.neighbors[2][1] = NULL;
                new_cell.neighbors[2][2] = NULL;
            }
            if (col == 0) {
                new_cell.neighbors[0][0] = NULL;
                new_cell.neighbors[1][0] = NULL;
                new_cell.neighbors[2][0] = NULL;
            }
            if (col == cols - 1) {
                new_cell.neighbors[0][2] = NULL;
                new_cell.neighbors[1][2] = NULL;
                new_cell.neighbors[2][2] = NULL;
            }
            grid[grid_index(row, col, cols)] = new_cell;
        }
    }

    unsigned center_x = cols / 2;
    unsigned center_y = rows / 2;
    int x, y;

    unsigned num_tuple_matches = 4;
    regmatch_t matches[num_tuple_matches];
    while (regexec(&input_tuple_regp, grid_input, num_tuple_matches, matches, 0) != REG_NOMATCH) {
        for (int curr_match = 1; curr_match < num_tuple_matches; curr_match++) {
            unsigned match_length = matches[curr_match].rm_eo - matches[curr_match].rm_so;
            char match_str[match_length + 1];
            match_str[match_length] = 0;
            memcpy(match_str, grid_input + matches[curr_match].rm_so, match_length);
            switch (curr_match) {
                case 1:
                    x = atoi(match_str);
                    break;
                case 2:
                    y = atoi(match_str);
                    break;
                case 3:
                    unsigned init_state = atoi(match_str);
                    grid[grid_index(center_y - y, center_x + x, cols)].state = init_state;
                    break;
            }
        }
        grid_input += matches[num_tuple_matches - 1].rm_eo;
    }

    regfree(&input_tuple_regp);

    return grid;
}

void step_grid(Cell* grid, unsigned rows, unsigned cols, CARuleset rules) {
    Cell* new_grid = malloc(sizeof(Cell) * rows * cols);

    for (unsigned row = 0; row < rows; row++) {
        for (unsigned col = 0; col < cols; col++) {
            Cell orig_cell = grid[grid_index(row, col, cols)];
            new_grid[grid_index(row, col, cols)] = step_cell(orig_cell, rules);
        }
    }

    for (unsigned row = 0; row < rows; row++) {
        for (unsigned col = 0; col < cols; col++) {
            Cell* orig_cell = grid + grid_index(row, col, cols);
            memcpy(orig_cell, &new_grid[grid_index(row, col, cols)], sizeof(Cell));
        }
    }

    free(new_grid);
}

void draw_grid(Cell* grid, WINDOW* window, unsigned rows, unsigned cols, CARuleset rules) {
    clear();

    for (unsigned row = 0; row < rows; row++) {
        for (unsigned col = 0; col < cols; col++) {
            Cell curr_cell = grid[grid_index(row, col, cols)];
            if (has_colors()) attron(COLOR_PAIR(curr_cell.state));
            mvwaddch(window, row, col, ' ');
            if (has_colors()) attroff(COLOR_PAIR(curr_cell.state));
        }
    }
    

    refresh();
}
