#include <ncurses.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <regex.h>
#include <string.h>

#include "grid.h"
#include "states.h"
#include "ruleset.h"

#define RULESET_LINE_REGEX	"[0-9]+"

int main(int argc, const char* argv[]) {
	assert(argc == 2);

	int exit_code = 0;

	unsigned rows, cols;

	WINDOW* window = initscr();
	noecho();
	curs_set(FALSE);
	getmaxyx(window, rows, cols);

	FILE* file_in = fopen(argv[1], "rb");

	if (file_in) {
		fseek(file_in, 0, SEEK_END);
		long length = ftell(file_in);
		fseek(file_in, 0, SEEK_SET);
		char file_in_chars[length + 1];
		file_in_chars[length] = 0;
		long read_length = fread(&file_in_chars, 1, length, file_in);
		fclose(file_in);

		assert(read_length == length);

		regex_t input_ruleset_regp;
		regmatch_t matches[1];

		assert(regcomp(&input_ruleset_regp, RULESET_LINE_REGEX, REG_EXTENDED) == 0);

		if (regexec(&input_ruleset_regp, file_in_chars, 1, matches, 0) == REG_NOMATCH) exit(-1);

		regfree(&input_ruleset_regp);

		size_t rule_str_len = matches[0].rm_eo - matches[0].rm_so;
		char rule_str_buf[rule_str_len + 1];
		memcpy(rule_str_buf, file_in_chars, rule_str_len);
		rule_str_buf[rule_str_len] = 0;

		CARuleset rules = atol(rule_str_buf);

		Cell* grid = init_grid(file_in_chars + matches[0].rm_eo, length, rows, cols);

		assert(grid != NULL);

		if (has_colors()) {
			start_color();

			switch (rules) {
				case CONWAY:
					init_pair(CGL_DEAD, COLOR_BLACK, COLOR_BLACK);
					init_pair(CGL_ALIVE, COLOR_WHITE, COLOR_WHITE);
					break;

				case FALLING_BLOCKS:
					init_pair(FB_EMPTY, COLOR_BLACK, COLOR_BLACK);
					init_pair(FB_BLOCK, COLOR_WHITE, COLOR_WHITE);
					break;

				case BRIANS_BRAIN:
					init_pair(BB_DEAD, COLOR_BLACK, COLOR_BLACK);
					init_pair(BB_ALIVE, COLOR_WHITE, COLOR_WHITE);
					init_pair(BB_DYING, COLOR_BLUE, COLOR_BLUE);
					break;

				case WIRE_WORLD:
					init_pair(WW_EMPTY, COLOR_BLACK, COLOR_BLACK);
					init_pair(WW_ELECTRON_HEAD, COLOR_BLUE, COLOR_BLUE);
					init_pair(WW_ELECTRON_TAIL, COLOR_RED, COLOR_RED);
					init_pair(WW_CONDUCTOR, COLOR_YELLOW, COLOR_YELLOW);
			}
		}

		draw_grid(grid, window, rows, cols, rules);

		while (1) {
			if (getch() == 4) break; // CTRL-D
			step_grid(grid, rows, cols, rules);
			draw_grid(grid, window, rows, cols, rules);
		}

		free(grid);
	} else {
		exit_code = -1;
	}

	endwin();

	return exit_code;
}