#ifndef GRID_H
#define GRID_H

#include <ncurses.h>

#include "cell.h"
#include "ruleset.h"

#define INPUT_TUPLE_REGEX "(-?[0-9]+)\\s+(-?[0-9]+)\\s+(-?[0-9]+)"

unsigned grid_index(unsigned, unsigned, unsigned);
Cell* init_grid(char*, long, unsigned, unsigned);
void free_grid(Cell*, unsigned, unsigned);
void step_grid(Cell*, unsigned, unsigned, CARuleset);
void draw_grid(Cell*, WINDOW*, unsigned, unsigned, CARuleset);

#endif